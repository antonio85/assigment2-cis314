//Antonio Silva Paucar

#include <stdio.h>

int mask(int n)
{
	//Ideas
 /*2^0 =1
 2^1 =2
 2^2 =4
 2^3 = 8
 2^4 = 16
 2^5 = 32
 2^6 = 64
 2^7 = 128   //  powers of two, it

 mask(1) 0000 0000 0000 0000 0000 0000 0000 0001 = 1 =2^1-1
 mask(2) 0000 0000 0000 0000 0000 0000 0000 0011 = 3 = 2^2-1
 mask(3) 0000 0000 0000 0000 0000 0000 0000 0111 = 7 =2^3-1
 mask(5) 0000 0000 0000 0000 0000 0000 0001 1111 = 31 =2^4-1
 mask(8) 0000 0000 0000 0000 0000 0000 1111 1111 = 255 = 2^8-1
 mask(16)0000 0000 0000 0000 1111 1111 1111 1111 = 65535 2^16-1
 mask(31)0111 1111 1111 1111 1111 1111 1111 1111 = 2147483647 2^32-1

 relation is 2^n-1 == 1<<n
 We use firs the number one binary and shift it to the left as many times the number of least significant bits we want. This gives us a power of 2, which we will subtract 1 to
 obtain a number whose binary equivalence could act as a mask.
*/


      return  (1<<n)-1; /*we use n as a power of 2 (we shift 1 (binary) to the left as many times the power we want)
                                  and then we subtract 1. The binary of that number will be a mask to cover return the least significant n numbers.*/
}

int main(void)
{
      int ejemplo;

       for(int i = 0; i<32;i++){    // I used the loop only to show how the program output the right answer.
         int ejemplo = mask(i);
         printf("%d: ",i);
         printf("%#10x  \n", ejemplo);
       }
}
