//Antonio Silva Paucar

#include <stdio.h>

int ge(float x, float y) {
    
    unsigned int ux = *((unsigned int*) &x); // convert x raw bits
    unsigned int uy = *((unsigned int*) &y); // convert y raw bits
    unsigned int sx = ux >> 31; // extract sign bit of ux
    unsigned int sy = uy >> 31; // extract sign bit of uy
    ux <<= 1; // drop sign bit of ux        ux=ux<<1 same
    uy <<= 1; // drop sign bit of uy        uy=uy<<1 same
    // TODO: return using sx, sy, ux, uy
    /*
// ideas
 

 -1.0f = 10111111100000000000000000000000
  0.0f = 00000000000000000000000000000000
 
  sx =   11111111111111111111111111111111     -1.0f shift >>31  sign  negative
  sy =   00000000000000000000000000000000                             positive
 
  ux<<=1 01111111000000000000000000000000
  uy<<=1 00000000000000000000000000000000
  ux>=uy ----> 1>0    true 1.
 -----------------------------------------------------------
  0.0f = 00000000000000000000000000000000
 -1.0f = 10111111100000000000000000000000
 
  sx   = 00000000000000000000000000000000 signed positive  sx>sy    0 is greater
  sy   = 11111111111111111111111111111111 signed negative
 
  ux   = 00000000000000000000000000000000   <<1    !(!ux)   0  ux<uy      1 is greater
  uy   = 01111111000000000000000000000000   <<1    !(!uy)   1
 */
    return ((!(!sx))<(!(!ux)) && ((!(!ux))>(!(!uy))));
    // (!(!sx)<!(!ux) && (!(!ux)>!(!uy)));
    //((!(!0))<(!(!1)) && ((!(!0))>(!(!1)));
    // (!(1)<(!0)&&(!1)>(!0)
    //
}

int main(void)
{
    
    int a = ge(0.0f, -1.0f);
    printf("%d\n", a);


}


/* EXPLORING
int main (void)
{
    float primero = -1.0f;
    
    unsigned int a = *((unsigned int*)&primero);
    printf("%#x\n",a);
    
    unsigned int b = a>>31;
    printf("%#x\n",b);
    
    unsigned int c =  b<<=1;
    printf("%#x\n",c);
    return 0;
}*/
