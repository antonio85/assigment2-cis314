//Antonio Silva Paucar

#include <stdio.h>

unsigned extract(unsigned int x, int i)
{
/*
resources: How to print leading zeros: https://stackoverflow.com/questions/12062916/printing-leading-zeroes-for-hexadecimal-in-c
//Ideas

0x12345678
  cero shift
  00001100 00100010 00111000 01001110
  00000000 00000000 00000000 11111111 shift 0 and &
  -----------------------------------
  00000000 00000000 00000000 01001110


dos shift

  00001100 11111111 00111000 01001110
  00000000 00000000 00000000 11111111 shift 16 << left
  00000000 11111111 00000000 00000000 &
  -----------------------------------
  00000000 11111111 00000000 00000000  shift left << 8    primero
  11111111 00000000 00000000 00000000  shift right >> 24   segundo
  11111111 11111111 11111111 11111111 result

*/
//int mask =0x000000FF<<(i<<3); // creates a mask of 00000000 00000000 00000000 00000000
//printf("Mask 0x%08x\n",mask);
//int primero = x&0x000000FF<<(i<<3);/ shift the mask according of the section that we want to isolate
//printf("Primero 0x%08x\n",primero);  debugging
//int segundo = primero << (24-(i<<3));//shift the 1's areas all the way to the left
//printf("Segundo 0x%08x\n",segundo); debugging
//int final =segundo>>24;// shift the 1's all the way to the right. Since it is an arithmetical shift, if the leading bit is one, this is extended, if it is zero, the same.

int primero = x&0x000000FF<<(i<<3);   //the value enters to the function and it is combine with the mask FF(which will be shifted the places the value we want to isolated.
                                                                //The result will be the isolated numbers, which will be move to the right as many times needed to reach the very end of the left side.
                                                                // after that, we will shift to the right until the isolated values gets to the zero place (the beginning from the right). Since this is an arithmetic
                                                                // shift, if the value starts with 1, the shift will drag that 1 all the way back to the right. In other hand, if it is zero, the shift will just keep everything a zero when it goes to the right.
return primero << (24-(i<<3))  >>24;//a mix of all the steps  above.
}

int main(void)
{
  printf("0x%08X\n",extract(0x12345678,0));
  printf("0x%08X\n",extract(0xABCDEF00,2));

}
